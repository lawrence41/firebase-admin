use crate::value::FirestoreValue;
use firestore_grpc::v1::Document;

pub struct FirestoreDocument(pub String, pub FirestoreValue);

impl From<Document> for FirestoreDocument {
    fn from(document: Document) -> Self {
        let name: String = document.name;
        let inner_value: FirestoreValue = document.fields.into();
        Self(name, inner_value)
    }
}

impl FirestoreDocument {}
