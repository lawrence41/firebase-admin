extern crate firestore_grpc;

use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

use firestore_grpc::google::firestore::v1::{value::ValueType, ArrayValue, MapValue, Value};

#[derive(Debug, Clone)]
pub struct FirestoreValue(pub Value);

impl From<i64> for FirestoreValue {
    fn from(value: i64) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::IntegerValue(value)),
        })
    }
}

impl From<HashMap<String, Value>> for FirestoreValue {
    fn from(value: HashMap<String, Value>) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::MapValue(MapValue { fields: value })),
        })
    }
}

impl From<f64> for FirestoreValue {
    fn from(value: f64) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::DoubleValue(value)),
        })
    }
}

impl From<bool> for FirestoreValue {
    fn from(value: bool) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::BooleanValue(value)),
        })
    }
}

impl From<String> for FirestoreValue {
    fn from(value: String) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::StringValue(value)),
        })
    }
}

impl From<&str> for FirestoreValue {
    fn from(value: &str) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::StringValue(value.to_string())),
        })
    }
}

impl FirestoreValue {}

impl From<Value> for FirestoreValue {
    fn from(v: Value) -> Self {
        FirestoreValue(v)
    }
}

impl From<&Value> for FirestoreValue {
    fn from(v: &Value) -> Self {
        FirestoreValue(v.clone())
    }
}

impl Deref for FirestoreValue {
    type Target = Value;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for FirestoreValue {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl TryInto<String> for FirestoreValue {
    type Error = ();
    fn try_into(self) -> Result<String, Self::Error> {
        match self.0.value_type.unwrap() {
            ValueType::StringValue(val) => Ok(val),
            _ => Err(()),
        }
    }
}

impl TryInto<bool> for FirestoreValue {
    type Error = ();
    fn try_into(self) -> Result<bool, Self::Error> {
        match self.0.value_type.unwrap() {
            ValueType::BooleanValue(val) => Ok(val),
            _ => Err(()),
        }
    }
}

impl TryInto<HashMap<String, Value>> for FirestoreValue {
    type Error = ();

    fn try_into(self) -> Result<HashMap<String, Value>, Self::Error> {
        match self.0.value_type.unwrap() {
            ValueType::MapValue(MapValue { fields }) => Ok(fields),
            _ => Err(()),
        }
    }
}

impl TryInto<i64> for FirestoreValue {
    type Error = ();
    fn try_into(self) -> Result<i64, Self::Error> {
        match self.0.value_type.unwrap() {
            ValueType::IntegerValue(val) => Ok(val),
            _ => Err(()),
        }
    }
}
impl<T> Into<Vec<T>> for FirestoreValue
where
    T: From<FirestoreValue>,
{
    fn into(self) -> Vec<T> {
        match self.0.value_type {
            Some(value_type) => match value_type {
                ValueType::ArrayValue(array_value) => array_value
                    .values
                    .into_iter()
                    .map(|v| T::from(FirestoreValue::from(v)))
                    .collect(),
                _ => panic!("Ah shit"),
            },
            None => panic!("Ah shit"),
        }
    }
}
impl<T> From<Vec<T>> for FirestoreValue
where
    T: Into<FirestoreValue>,
{
    fn from(values: Vec<T>) -> Self {
        FirestoreValue(Value {
            value_type: Some(ValueType::ArrayValue(ArrayValue {
                values: values.into_iter().map(|v| v.into().0).collect(),
            })),
        })
    }
}

#[cfg(test)]
pub mod test {
    use std::collections::HashMap;

    use firestore_grpc::google::firestore::v1::{value::ValueType, Value};

    use crate::value::FirestoreValue;

    fn new_integer_value(v: i64) -> Value {
        Value {
            value_type: Some(ValueType::IntegerValue(v)),
        }
    }

    #[test]
    pub fn value_conversion() {
        let value = new_integer_value(84);
        let firestore_value: FirestoreValue = value.into();
    }

    #[test]
    pub fn test_conversion() {
        let firestore_string_value: FirestoreValue = FirestoreValue::from("Hello");
        let string_value: String = firestore_string_value.try_into().unwrap();
        assert_eq!(string_value, "Hello");

        let firestore_int_value = FirestoreValue::from(new_integer_value(39));
        let int_value: i64 = firestore_int_value.clone().try_into().unwrap();
        assert_eq!(int_value, 39);

        let firestore_map_value = FirestoreValue::from(HashMap::from([(
            "int_value".to_string(),
            firestore_int_value.0,
        )]));
    }

    #[test]
    pub fn list_conversion() {
        let x: FirestoreValue =
            vec![FirestoreValue::from(24), FirestoreValue::from("Hello")].into();
        let y = FirestoreValue::from(24);
        println!("{:?} :: {:?}", y, x);
    }
}
