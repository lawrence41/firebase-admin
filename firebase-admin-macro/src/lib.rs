extern crate proc_macro;

use proc_macro::TokenStream;

use proc_macro2::Span;
use quote::quote;
use syn::{parse_macro_input, Data, DataStruct, DeriveInput, Ident};

#[proc_macro_derive(FromFirestoreValue)]
pub fn from_firestore_value(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let data: DataStruct = match input.data {
        Data::Struct(d) => d,
        _ => panic!("Only works on structs!!"),
    };
    let struct_ident = input.ident;

    let fields = data.fields.iter();
    let to_hash_map_assignments = fields.clone().map(|field| {
        let field_name = field.ident.as_ref().unwrap();
        quote! {
            (stringify!(#field_name).to_string(), firebase_admin::value::FirestoreValue::from(item.#field_name).0),
        }
    });
    let to_firebase_value_assignments = fields.clone().map(|field| {
        let field_name = field.ident.as_ref().unwrap();
        let field_type = &field.ty;
        quote! {
            let field_value =  firebase_admin::value::FirestoreValue::from(fields.get(stringify!(#field_name)).unwrap());
            let #field_name: #field_type = field_value.try_into().unwrap();
        }
    });

    let optional_declarations = fields.clone().map(|field| {
        let field_name = field.ident.as_ref().unwrap();
        let field_type = &field.ty;
        quote! {
            #field_name: Option<#field_type>,
        }
    });
    let optional_struct_name = format!("Optional{}", struct_ident);
    let optional_struct_ident = Ident::new(&optional_struct_name, Span::call_site());
    let optional_struct_open =
        quote! {  struct #optional_struct_ident { #(#optional_declarations)* } };

    let struct_literal_fields = fields.map(|field| {
        let field_name = field.ident.as_ref().unwrap();
        quote! {#field_name,}
    });
    let struct_literal = quote! {
        Self {
            #(#struct_literal_fields)*
        }
    };

    let tokens = quote! {



        #optional_struct_open

        impl From<FirestoreValue> for #struct_ident {
            fn from(firestore_value: firebase_admin::value::FirestoreValue ) -> Self {
                match firestore_value.0.value_type.unwrap() {
                firebase_admin::ValueType::MapValue(firebase_admin::MapValue { fields }) => {
                        #(#to_firebase_value_assignments)*
                        #struct_literal
                    },
                    _ => panic!("unknown object!"),
                }
            }
        }
        impl From<#struct_ident> for FirestoreValue {
            fn from(item: #struct_ident ) -> Self {
                let fields = std::collections::HashMap::from(
                    [#(#to_hash_map_assignments)*]
                );
                firebase_admin::value::FirestoreValue::from(fields)
            }
        }
    };
    tokens.into()
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
